##### Determinar 'n' utilizando MMQ! #####

#importando libs
import math
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import statsmodels.api as sm

#criando variáveis
#variável 'x' - Volume
x = np.array ([100000, 110000, 120000, 130000, 140000, 150000, 160000, 170000, 180000, 190000, 200000])
#variável 'y' - Pressão
y = np.array ([3, 2.7, 2.4, 2.2, 1.9, 1.7, 1.5, 1.3, 1.2, 1.1, 1])

#linearizando a equação pV^n=constante ultilizando log
i = 0
logV = ([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
logP = ([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
while (i < 11):
    logV[i] = math.log(x[i], 10)
    logP[i] = math.log(y[i], 10)
    i = i+1

#adicionando constante a matriz X
x_sm = sm.add_constant (logV)

#MMQ - OLS_Ordinary Least Squares
results = sm.OLS(logP, x_sm).fit()

#mostrando estatísticas
results.summary()

#previsões para o mesmo conjunto passado
results.predict(x_sm)

print (results.summary())